#!/bin/sh
# LNPM install shell
# By Zmor
# 2013-3-19





echo "1、安装Nginx："

yum -y install wget
yum -y install pcre
yum -y install openssl*
yum -y install gcc gcc-c++ autoconf libjpeg libjpeg-devel libpng libpng-devel freetype freetype-devel libxml2 libxml2-devel zlib zlib-devel glibc glibc-devel glib2 glib2-devel bzip2 bzip2-devel ncurses ncurses-devel curl curl-devel e2fsprogs e2fsprogs-devel krb5 krb5-devel libidn libidn-devel openssl openssl-devel openldap openldap-devel nss_ldap openldap-clients openldap-servers make
yum -y install gd gd2 gd-devel gd2-devel
/usr/sbin/groupadd www
/usr/sbin/useradd -g www www
ulimit -SHn 65535


#wget --no-check-certificate https://bitbucket.org/lure21/lnpm/downloads/pcre-8.32.tar.gz
tar zxvf pcre-8.32.tar.gz
cd pcre-8.32
./configure --prefix=/usr/local/webserver/pcre
make && make install
cd ../


#wget --no-check-certificate https://bitbucket.org/lure21/lnpm/downloads/ngx_cache_purge-2.1.tar.gz
tar zxvf ngx_cache_purge-2.1.tar.gz

#wget --no-check-certificate https://bitbucket.org/lure21/lnpm/downloads/nginx-1.5.2.tar.gz
tar zxvf nginx-1.5.2.tar.gz
cd nginx-1.5.2
./configure --user=www --group=www --prefix=/usr/local/webserver/nginx --with-http_stub_status_module --with-http_ssl_module --with-pcre=../pcre-8.32 --add-module=../ngx_cache_purge-2.1 --with-http_realip_module --with-http_image_filter_module
make
make install
cd ../


echo "创建Nginx日志目录"
mkdir -p /data/nginx/vhosts
mkdir -p /data/nginx/logs
chmod +w /data/nginx/logs
chown -R www:www /data/nginx/logs



echo "创建Nginx配置文件"
mv /usr/local/webserver/nginx/conf/nginx.conf /usr/local/webserver/nginx/conf/nginx.conf.bak
echo "user  www www;

worker_processes 8;

error_log  /data/nginx/logs/nginx_error.log  crit;

pid        /usr/local/webserver/nginx/nginx.pid;

#Specifies the value for maximum file descriptors that can be opened by this process.
worker_rlimit_nofile 65535;

events
{
  use epoll;
  worker_connections 65535;
}

http
{
  include       mime.types;
  default_type  application/octet-stream;

  #charset  gb2312;

  server_names_hash_bucket_size 128;
  client_header_buffer_size 32k;
  large_client_header_buffers 4 32k;
  client_max_body_size 8m;

  sendfile on;
  tcp_nopush     on;

  keepalive_timeout 60;

  tcp_nodelay on;

  fastcgi_connect_timeout 300;
  fastcgi_send_timeout 300;
  fastcgi_read_timeout 300;
  fastcgi_buffer_size 64k;
  fastcgi_buffers 4 64k;
  fastcgi_busy_buffers_size 128k;
  fastcgi_temp_file_write_size 128k;

  gzip on;
  gzip_min_length  1k;
  gzip_buffers     4 16k;
  gzip_http_version 1.0;
  gzip_comp_level 2;
  gzip_types       text/plain application/x-javascript text/css application/xml;
  gzip_vary on;

  #limit_zone  crawler  \$binary_remote_addr  10m;
  proxy_temp_path   /data/nginx/proxy_temp_dir;
  proxy_cache_path  /data/nginx/proxy_cache_dir  levels=1:2   keys_zone=cache_one:500m inactive=1d max_size=30g;

  log_format  access  '\$remote_addr - \$remote_user [\$time_local] \"\$request\" '
              '\$status \$body_bytes_sent \"\$http_referer\" '
              '\"\$http_user_agent\" \$http_x_forwarded_for';
  server
  {
    listen       80;
    server_name  localhost;
    index index.html index.htm index.php;
    root  /data/wwwroot/localhost;

    #limit_conn   crawler  20;

    location ~ .*\.(php|php5)?$
    {
      #fastcgi_pass  unix:/tmp/php-cgi.sock;
      fastcgi_pass  127.0.0.1:9000;
      fastcgi_index index.php;
      include fcgi.conf;
    }

    location ~ .*\.(gif|jpg|jpeg|png|bmp|swf)$
    {
      expires      30d;
    }

    location ~ .*\.(js|css)?$
    {
      expires      1h;
    }

    location /status
    {
    stub_status on;
    access_log   off;
    }

    access_log off;
    }

    include /data/nginx/vhosts/*.conf;

}" > /usr/local/webserver/nginx/conf/nginx.conf

echo "在/usr/local/webserver/nginx/conf/目录中创建fcgi.conf文件："
echo 'if ($request_filename ~* (.*)\.php) {
    set $php_url $1;
}
if (!-e $php_url.php) {
    return 403;
}


fastcgi_param  GATEWAY_INTERFACE  CGI/1.1;
fastcgi_param  SERVER_SOFTWARE    nginx;

fastcgi_param  QUERY_STRING       $query_string;
fastcgi_param  REQUEST_METHOD     $request_method;
fastcgi_param  CONTENT_TYPE       $content_type;
fastcgi_param  CONTENT_LENGTH     $content_length;

fastcgi_param  SCRIPT_FILENAME    $document_root$fastcgi_script_name;
fastcgi_param  SCRIPT_NAME        $fastcgi_script_name;
fastcgi_param  REQUEST_URI        $request_uri;
fastcgi_param  DOCUMENT_URI       $document_uri;
fastcgi_param  DOCUMENT_ROOT      $document_root;
fastcgi_param  SERVER_PROTOCOL    $server_protocol;

fastcgi_param  REMOTE_ADDR        $remote_addr;
fastcgi_param  REMOTE_PORT        $remote_port;
fastcgi_param  SERVER_ADDR        $server_addr;
fastcgi_param  SERVER_PORT        $server_port;
fastcgi_param  SERVER_NAME        $server_name;

# PHP only, required if PHP was built with --enable-force-cgi-redirect
fastcgi_param  REDIRECT_STATUS    200;
' > /usr/local/webserver/nginx/conf/fcgi.conf


echo "启动Nginx"
/usr/local/webserver/nginx/sbin/nginx

echo "配置开机自动启动Nginx"
echo '

ulimit -SHn 65535
/usr/local/webserver/nginx/sbin/nginx
' >> /etc/rc.local

echo "编写每天定时切割Nginx日志的脚本"
echo '#!/bin/bash
# This script run at 00:00

# The Nginx logs path
logs_path="/data/nginx/logs/"
logs_to_path="/data/nginx/logs_backup/$(date -d 'yesterday' +'%Y')/$(date -d 'yesterday' +'%m')/$(date -d 'yesterday' +'%Y%m%d')/"

#delete 3 day ago logs.
logs_del_path="/data/nginx/logs_backup/$(date -d '3 days ago' +'%Y')/$(date -d '3 days ago' +'%m')/$(date -d '3 days ago' +'%Y%m%d')/"
rm -rf ${logs_del_path}

mkdir -p ${logs_to_path}
mv ${logs_path}*.log ${logs_to_path}
kill -USR1 `cat /usr/local/webserver/nginx/nginx.pid`
' > /usr/local/webserver/nginx/sbin/cut_nginx_log.sh
chmod +x /usr/local/webserver/nginx/sbin/cut_nginx_log.sh
echo '
00 00 * * * root /usr/local/webserver/nginx/sbin/cut_nginx_log.sh
' >> /etc/crontab




echo "2、安装 MySQL："

echo '创建MySQL数据库存放目录'
mkdir -p /data/mysql/3306/data/
mkdir -p /data/mysql/3306/binlog/
mkdir -p /data/mysql/3306/relaylog/
chown -R mysql:mysql /data/mysql/


#wget --no-check-certificate https://bitbucket.org/lure21/lnpm/downloads/mysql-5.6.10-linux-glibc2.5-x86_64.tar.gz
tar zxvf mysql-5.6.10-linux-glibc2.5-x86_64.tar.gz
mv mysql-5.6.10-linux-glibc2.5-x86_64 /usr/local/webserver/mysql
/usr/sbin/groupadd mysql
/usr/sbin/useradd -g mysql mysql
mkdir -p /data/mysql/3306/data
yum -y install libaio
/usr/local/webserver/mysql/scripts/mysql_install_db --basedir=/usr/local/webserver/mysql --datadir=/data/mysql/3306/data --user=mysql

sed -i "s#/usr/local/mysql#/usr/local/webserver/mysql#g" /usr/local/webserver/mysql/bin/mysqld_safe


echo '创建配置文件&启动脚本'
echo '[client]
character-set-server = utf8
port    = 3306
socket  = /tmp/mysql.sock

[mysqld]
character-set-server = utf8
user    = mysql
port    = 3306
socket  = /tmp/mysql.sock
basedir = /usr/local/webserver/mysql
datadir = /data/mysql/3306/data
log-error = /data/mysql/3306/mysql_error.log
pid-file = /data/mysql/3306/mysql.pid
open_files_limit    = 10240
back_log = 600
max_connections = 5000
max_connect_errors = 6000
#table_cache = 614
external-locking = FALSE
max_allowed_packet = 32M
sort_buffer_size = 1M
join_buffer_size = 1M
thread_cache_size = 300
#thread_concurrency = 8
query_cache_size = 512M
query_cache_limit = 2M
query_cache_min_res_unit = 2k
default-storage-engine = MyISAM
thread_stack = 192K
transaction_isolation = READ-COMMITTED
tmp_table_size = 246M
max_heap_table_size = 246M
long_query_time = 3
log-bin = /data/mysql/3306/binlog/binlog
binlog_cache_size = 4M
binlog_format = MIXED
max_binlog_cache_size = 8M
max_binlog_size = 1G
relay-log-index = /data/mysql/3306/relaylog/relaylog
relay-log-info-file = /data/mysql/3306/relaylog/relaylog
relay-log = /data/mysql/3306/relaylog/relaylog
expire_logs_days = 30
key_buffer_size = 256M
read_buffer_size = 1M
read_rnd_buffer_size = 16M
bulk_insert_buffer_size = 64M
myisam_sort_buffer_size = 128M
myisam_max_sort_file_size = 10G
myisam_repair_threads = 1
myisam_recover

interactive_timeout = 60
wait_timeout = 60

skip-name-resolve
#master-connect-retry = 10
slave-skip-errors = 1032,1062,126,1114,1146,1048,1396,1053


#master-connect-retry=10
# replicate config


#skip-innodb
#innodb_additional_mem_pool_size = 16M
#innodb_buffer_pool_size = 512M
#innodb_data_file_path = ibdata1:256M:autoextend
#innodb_file_io_threads = 4
#innodb_thread_concurrency = 8
#innodb_flush_log_at_trx_commit = 2
#innodb_log_buffer_size = 16M
#innodb_log_file_size = 128M
#innodb_log_files_in_group = 3
#innodb_max_dirty_pages_pct = 90
#innodb_lock_wait_timeout = 120
#innodb_file_per_table = 0

#log-slow-queries = /data/mysql/3306/slow.log
#long_query_time = 10

[mysqldump]
quick
max_allowed_packet = 32M
' > /data/mysql/3306/my.cnf

echo '#!/bin/sh

mysql_port=3306
mysql_username="root"
mysql_password="159357"

function_start_mysql()
{
    printf "Starting MySQL...\n"
    /bin/sh /usr/local/webserver/mysql/bin/mysqld_safe --defaults-file=/data/mysql/${mysql_port}/my.cnf 2>&1 > /dev/null &
}

function_stop_mysql()
{
    printf "Stoping MySQL...\n"
    /usr/local/webserver/mysql/bin/mysqladmin -u ${mysql_username} -p${mysql_password} -S /tmp/mysql.sock shutdown
}

function_restart_mysql()
{
    printf "Restarting MySQL...\n"
    function_stop_mysql
    sleep 5
    function_start_mysql
}

function_kill_mysql()
{
    kill -9 $(ps -ef | grep "bin/mysqld_safe" | grep ${mysql_port} | awk "{printf $2}")
    kill -9 $(ps -ef | grep "libexec/mysqld" | grep ${mysql_port} | awk "{printf $2}")
}

if [ "$1" = "start" ]; then
    function_start_mysql
elif [ "$1" = "stop" ]; then
    function_stop_mysql
elif [ "$1" = "restart" ]; then
function_restart_mysql
elif [ "$1" = "kill" ]; then
function_kill_mysql
else
    printf "Usage: /data/mysql/${mysql_port}/mysql {start|stop|restart|kill}\n"
fi
' > /data/mysql/3306/mysql

chmod +x /data/mysql/3306/mysql
chown -R mysql:mysql /data/mysql/

echo '启动MySQL'
/data/mysql/3306/mysql start

sleep 5

/usr/local/webserver/mysql/bin/mysqladmin -u root password '159357'

echo '
/data/mysql/3306/mysql start
' >> /etc/rc.local



echo "3、安装PHP依赖库"

mkdir -p /usr/local/webserver/libs/
#wget --no-check-certificate https://bitbucket.org/lure21/lnpm/downloads/jpegsrc.v9.tar.gz     
tar zxvf jpegsrc.v9.tar.gz
cd jpeg-9/
./configure --prefix=/usr/local/webserver/libs --enable-shared --enable-static --prefix=/usr/local/webserver/libs
make
make install
cd ../

#wget --no-check-certificate https://bitbucket.org/lure21/lnpm/downloads/libpng-1.6.2.tar.gz
tar zxvf libpng-1.6.2.tar.gz
cd libpng-1.6.2/
./configure --prefix=/usr/local/webserver/libs
make
make install
cd ../

#wget --no-check-certificate https://bitbucket.org/lure21/lnpm/downloads/freetype-2.4.12.tar.gz
tar zxvf freetype-2.4.12.tar.gz
cd freetype-2.4.12/
./configure --prefix=/usr/local/webserver/libs
make
make install
cd ../


#wget --no-check-certificate https://bitbucket.org/lure21/lnpm/downloads/mhash-0.9.9.9.tar.gz
#wget --no-check-certificate https://bitbucket.org/lure21/lnpm/downloads/libmcrypt-2.5.8.tar.gz
#wget --no-check-certificate https://bitbucket.org/lure21/lnpm/downloads/mcrypt-2.6.8.tar.gz


tar zxvf libmcrypt-2.5.8.tar.gz*
cd libmcrypt-2.5.8/
./configure --prefix=/usr/local/webserver/libs
make
make install
cd libltdl/
./configure --prefix=/usr/local/webserver/libs --enable-ltdl-install
make
make install
cd ../../


tar zxvf mhash-0.9.9.9.tar.gz*
cd mhash-0.9.9.9/
./configure --prefix=/usr/local/webserver/libs
make
make install
cd ../




echo "vi /etc/ld.so.conf 添加：/usr/local/webserver/libs/lib"

echo "
/usr/local/webserver/libs/lib
/usr/local/webserver/mysql/lib" >> /etc/ld.so.conf

echo "然后：ldconfig"
ldconfig

tar zxvf mcrypt-2.6.8.tar.gz*
cd mcrypt-2.6.8/
export LDFLAGS="-L/usr/local/webserver/libs/lib -L/usr/lib"
export CFLAGS="-I/usr/local/webserver/libs/include -I/usr/include"
touch malloc.h
./configure --prefix=/usr/local/webserver/libs --with-libmcrypt-prefix=/usr/local/webserver/libs
make
make install
cd ../


echo "4、编译安装PHP 5.5"
#wget --no-check-certificate https://bitbucket.org/lure21/lnpm/downloads/php-5.5.9.tar.gz
tar zxvf php-5.5.9.tar.gz
cd php-5.5.*/
export LIBS="-lm -ltermcap -lresolv"
export DYLD_LIBRARY_PATH="/usr/local/webserver/mysql/lib/:/lib/:/usr/lib/:/usr/local/lib:/lib64/:/usr/lib64/:/usr/local/lib64"
export LD_LIBRARY_PATH="/usr/local/webserver/mysql/lib/:/lib/:/usr/lib/:/usr/local/lib:/lib64/:/usr/lib64/:/usr/local/lib64"
./configure --prefix=/usr/local/webserver/php --with-config-file-path=/usr/local/webserver/php/etc --with-mysql=/usr/local/webserver/mysql --with-mysqli=/usr/local/webserver/mysql/bin/mysql_config --with-iconv-dir --with-freetype-dir=/usr/local/webserver/libs --with-jpeg-dir=/usr/local/webserver/libs --with-png-dir=/usr/local/webserver/libs --with-zlib --with-libxml-dir=/usr --enable-xml --disable-rpath --enable-bcmath --enable-shmop --enable-sysvsem --enable-inline-optimization --with-curl --enable-mbregex --enable-fpm --enable-mbstring --with-mcrypt=/usr/local/webserver/libs --with-gd --enable-gd-native-ttf --with-openssl --with-mhash --enable-pcntl --enable-sockets --with-xmlrpc --enable-zip --enable-soap --enable-opcache --with-pdo-mysql --enable-maintainer-zts
make
make install
cp php.ini-development /usr/local/webserver/php/etc/php.ini
cd ../


ln -s /usr/local/webserver/mysql/lib/libmysqlclient.18.dylib /usr/lib/libmysqlclient.18.dylib
cp /usr/local/webserver/php/etc/php-fpm.conf /usr/local/webserver/php/etc/php-fpm.conf.default
sed -i 's#user = nobody#user = www#' /usr/local/webserver/php/etc/php-fpm.conf
sed -i 's#group = nobody#group = www#' /usr/local/webserver/php/etc/php-fpm.conf
sed -i 's#;pid = run/php-fpm.pid#pid = run/php-fpm.pid#' /usr/local/webserver/php/etc/php-fpm.conf
sed -i 's#;error_log = log/php-fpm.log#error_log = log/php-fpm.log#' /usr/local/webserver/php/etc/php-fpm.conf



echo "创建www用户和组，以及供虚拟主机使用的目录："
mkdir -p /data/wwwroot/localhost /data/nginx/proxy_temp_dir /data/nginx/proxy_cache_dir
chmod +w /data/wwwroot/localhost /data/nginx/proxy_temp_dir /data/nginx/proxy_cache_dir
chown -R www:www /data/wwwroot/localhost /data/nginx/proxy_temp_dir /data/nginx/proxy_cache_dir
echo "<?php phpinfo(); ?>" > /data/wwwroot/localhost/_info.php



echo "5、编译安装PHP扩展"

#wget --no-check-certificate https://bitbucket.org/lure21/lnpm/downloads/autoconf-latest.tar.gz
tar zxvf autoconf-latest.tar.gz
cd autoconf-2.69/
./configure --prefix=/usr/local/webserver/libs
make
make install
cd ../

export PHP_AUTOCONF="/usr/local/webserver/libs/bin/autoconf"
export PHP_AUTOHEADER="/usr/local/webserver/libs/bin/autoheader"


#wget --no-check-certificate https://bitbucket.org/lure21/lnpm/downloads/memcache-2.2.7.tgz
tar zxvf memcache-2.2.7.tgz
cd memcache-2.2.7/
/usr/local/webserver/php/bin/phpize
./configure --with-php-config=/usr/local/webserver/php/bin/php-config
make
make install
cd ../


echo "PHP真正地支持多线程扩展安装"
#wget --no-check-certificate https://bitbucket.org/lure21/lnpm/downloads/pthreads.tar.gz
tar -zxvf pthreads.tar.gz
cd krakjoe-pthreads*
/usr/local/webserver/php/bin/phpize
./configure --with-php-config=/usr/local/webserver/php/bin/php-config
make
make install
cd ../


echo "PHP oci8支持"
#wget --no-check-certificate https://bitbucket.org/lure21/lnpm/downloads/oracle-instantclient11.1-basic-11.1.0.7.0-1.x86_64.rpm
#wget --no-check-certificate https://bitbucket.org/lure21/lnpm/downloads/oracle-instantclient11.1-devel-11.1.0.7.0-1.x86_64.rpm
#wget --no-check-certificate https://bitbucket.org/lure21/lnpm/downloads/oracle-instantclient11.1-sqlplus-11.1.0.7.0-1.x86_64.rpm
rpm -ivh oracle-instantclient11.1*
echo "/usr/lib/oracle/11.1/client64/lib/" > /etc/ld.so.conf.d/oracle_client.conf
/sbin/ldconfig
#wget --no-check-certificate https://bitbucket.org/lure21/lnpm/downloads/oci8-2.0.8.tgz
tar zxvf oci8-2.0.8.tgz
cd oci8-2.0.8/
/usr/local/webserver/php/bin/phpize
CFLAGS="-I/usr/include/oracle/11.1/client64/"
CXXFLAGS="-I/usr/include/oracle/11.1/client64/"
./configure --with-php-config=/usr/local/webserver/php/bin/php-config --with-oci8=instantclient,/usr/lib/oracle/11.1/client64/lib/
make
make install
cd ../



echo '打开 /usr/local/webserver/php/etc/php.ini 查找 ; extension_dir = "ext"
　　在其后增加一行：
extension = "memcache.so"
extension = "pthreads.so"
'

cp /usr/local/webserver/php/etc/php.ini /usr/local/webserver/php/etc/php.ini.bak
sed -i 's#;     session.save_path = "N;/path"#session.save_path = /tmp#' /usr/local/webserver/php/etc/php.ini
sed -i 's#extension_dir = "ext"#extension_dir = "ext"\nextension = "memcache.so"\nextension = "pthreads.so"\nextension = "oci8.so"#' /usr/local/webserver/php/etc/php.ini


echo '创建php-fpm.sh启动脚本'
echo '#!/bin/bash  
#  
#processname: php-fpm
#author: Zmor
#date: 20140307
 
path=/usr/local/webserver/php/sbin/php-fpm
 
case $1 in
       start)
              echo -n "Starting php-fpm"
              $path
              echo " done"
       ;;
 
       stop)
              echo -n "Stopping php-fpm"
              kill -INT `cat /usr/local/webserver/php/var/run/php-fpm.pid`
              echo " done"
       ;;
 
       restart)
       	      echo -n "Restarting php-fpm"
              kill -USR2 `cat /usr/local/webserver/php/var/run/php-fpm.pid`
	      echo " done"
       ;;
 
       show)
	      netstat -anp|grep 9000
       ;;
 
       *)
              echo -n "Usage: $0 {start|stop|restart|show}\n"
       ;;
 
esac
' > /usr/local/webserver/php/sbin/php-fpm.sh

chmod +x /usr/local/webserver/php/sbin/php-fpm.sh

/usr/local/webserver/php/sbin/php-fpm.sh start

echo '/usr/local/webserver/php/sbin/php-fpm.sh start' >> /etc/rc.local







echo "优化Linux内核参数"
echo '
# Add
net.ipv4.tcp_max_syn_backlog = 65536
net.core.netdev_max_backlog =  32768
net.core.somaxconn = 32768

net.core.wmem_default = 8388608
net.core.rmem_default = 8388608
net.core.rmem_max = 16777216
net.core.wmem_max = 16777216

net.ipv4.tcp_timestamps = 0
net.ipv4.tcp_synack_retries = 2
net.ipv4.tcp_syn_retries = 2

net.ipv4.tcp_tw_recycle = 1
#net.ipv4.tcp_tw_len = 1
net.ipv4.tcp_tw_reuse = 1

net.ipv4.tcp_mem = 94500000 915000000 927000000
net.ipv4.tcp_max_orphans = 3276800

#net.ipv4.tcp_fin_timeout = 30
#net.ipv4.tcp_keepalive_time = 120
net.ipv4.ip_local_port_range = 1024  65535
' >> /etc/sysctl.conf

/sbin/sysctl -p


echo "Done."